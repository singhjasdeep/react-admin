/*
 * @file: constants.js
 * @description: It Contain action types Related Action.
 * @author: Jasdeep Singh
 */

/*********** USER ***********/
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOG_OUT = 'LOG_OUT';

/************ LOADING *************/
export const IS_LOADING = 'IS_LOADING';

/*********** COMPETITION ***********/
export const ADD_COMPETITION = 'ADD_COMPETITION';
export const GET_COMPETITION = 'GET_COMPETITION';
export const UPDATE_COMPETITION = 'UPDATE_COMPETITION';
export const DELETE_COMPETITION = 'DELETE_COMPETITION';